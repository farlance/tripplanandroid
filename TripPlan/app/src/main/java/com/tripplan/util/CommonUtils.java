package com.tripplan.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.widget.Toast;

public class CommonUtils {

    public static boolean hasPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int targetSdkVersion = 0;
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                targetSdkVersion = info.applicationInfo.targetSdkVersion;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (targetSdkVersion >= Build.VERSION_CODES.M) {
                return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
            }

            return PermissionChecker.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        }

        return context.checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity context, String permissionName, int permissionRequestCode) {
        String[] requestPermissions;

        requestPermissions = new String[]{permissionName};

        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                permissionName)) {
            Toast.makeText(context, "Permission Rationale", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(context, requestPermissions,
                    permissionRequestCode);
        } else {
            ActivityCompat.requestPermissions(context, requestPermissions,
                    permissionRequestCode);
        }
    }

}
