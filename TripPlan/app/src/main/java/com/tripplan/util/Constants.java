package com.tripplan.util;

public class Constants {

    public static final String KEY_PLACES_LIST = "key_places_list";

    public static final String BEACH_PLAN = "0";
    public static final String MOUNTAIN_PLAN = "1";
    public static final String HISTORY_TOURISM_PLAN = "2";
    public static final String TREKKING_SPOTS_PLAN = "3";

}
