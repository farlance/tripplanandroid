package com.tripplan.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "TripPlan.db";
    public static final String PLACES_TABLE_NAME = "places";
    public static final String PLACE_ID = "id";
    public static final String PLACE_NAME = "placeName";
    public static final String PLACE_DESC = "distance";
    public static final String PLACE_IMG_URL = "imgUrl";
    public static final String PLACE_CATEGORY = "category";
    public static final String PLACE_VALUE = "value";
    private HashMap hp;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("create table " + PLACES_TABLE_NAME + " (" + PLACE_ID + " text primary key, " +
                PLACE_NAME + " text, " +
                PLACE_DESC + " text, " +
                PLACE_IMG_URL + " text, " +
                PLACE_CATEGORY + " text, " +
                PLACE_VALUE + " integer ) "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS places");
        onCreate(db);
    }

    public boolean insertPlaces(List<Places> placesList) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < placesList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PLACE_ID, placesList.get(i).getId());
            contentValues.put(PLACE_NAME, placesList.get(i).getPlaceName());
            contentValues.put(PLACE_DESC, placesList.get(i).getPlaceDisp());
            contentValues.put(PLACE_IMG_URL, placesList.get(i).getImgUrl());
            contentValues.put(PLACE_CATEGORY, placesList.get(i).getCategory());
            contentValues.put(PLACE_VALUE, placesList.get(i).getValue());
            db.insert(PLACES_TABLE_NAME, null, contentValues);
        }
        db.close();
        return true;
    }

    public Cursor getData(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from places where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, PLACES_TABLE_NAME);
        return numRows;
    }

    public boolean updatePlace(List<Places> placesList) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < placesList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PLACE_ID, placesList.get(i).getId());
            contentValues.put(PLACE_NAME, placesList.get(i).getPlaceName());
            contentValues.put(PLACE_DESC, placesList.get(i).getPlaceDisp());
            contentValues.put(PLACE_IMG_URL, placesList.get(i).getImgUrl());
            contentValues.put(PLACE_CATEGORY, placesList.get(i).getCategory());
            contentValues.put(PLACE_VALUE, placesList.get(i).getValue());
            db.update("places", contentValues, "id = ? ", new String[]{placesList.get(i).getId()});
        }
        db.close();
        return true;
    }

    public Integer deletePlace(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("places",
                "id = ? ",
                new String[]{id});
    }

    public List<Places> getAllPlaces() {
        List<Places> placesList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from places", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            Places places = new Places();
            places.setId(res.getString(res.getColumnIndex(PLACE_ID)));
            places.setPlaceName(res.getString(res.getColumnIndex(PLACE_NAME)));
            places.setPlaceDisp(res.getString(res.getColumnIndex(PLACE_DESC)));
            places.setImgUrl(res.getString(res.getColumnIndex(PLACE_IMG_URL)));
            places.setCategory(res.getString(res.getColumnIndex(PLACE_CATEGORY)));
            places.setValue(res.getInt(res.getColumnIndex(PLACE_VALUE)));
            placesList.add(places);
            res.moveToNext();
        }
        db.close();
        return placesList;
    }

    public List<Places> getPlacesByCategory(String category) {
        List<Places> placesList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from places where category = " + category;
        Cursor res = db.rawQuery(query, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            Places places = new Places();
            places.setId(res.getString(res.getColumnIndex(PLACE_ID)));
            places.setPlaceName(res.getString(res.getColumnIndex(PLACE_NAME)));
            places.setPlaceDisp(res.getString(res.getColumnIndex(PLACE_DESC)));
            places.setImgUrl(res.getString(res.getColumnIndex(PLACE_IMG_URL)));
            places.setCategory(res.getString(res.getColumnIndex(PLACE_CATEGORY)));
            places.setValue(res.getInt(res.getColumnIndex(PLACE_VALUE)));
            placesList.add(places);
            res.moveToNext();
        }
        db.close();
        return placesList;
    }
}
