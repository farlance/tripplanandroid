package com.tripplan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlacesList {
    @SerializedName("List")
    @Expose
    private List<Places> placesList = null;

    public List<Places> getPlacesList() {
        return placesList;
    }

}
