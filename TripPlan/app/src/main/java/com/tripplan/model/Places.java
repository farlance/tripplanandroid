package com.tripplan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Places implements Parcelable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("placeName")
    @Expose
    private String placeName;
    @SerializedName("placeDisp")
    @Expose
    private String placeDisp;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("value")
    @Expose
    private Integer value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceDisp() {
        return placeDisp;
    }

    public void setPlaceDisp(String placeDisp) {
        this.placeDisp = placeDisp;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Places() {
    }

    protected Places(Parcel in) {
        id = in.readString();
        placeName = in.readString();
        placeDisp = in.readString();
        distance = in.readString();
        imgUrl = in.readString();
        category = in.readString();
        value = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(placeName);
        dest.writeString(placeDisp);
        dest.writeString(distance);
        dest.writeString(imgUrl);
        dest.writeString(category);
        if (value == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(value);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Places> CREATOR = new Parcelable.Creator<Places>() {
        @Override
        public Places createFromParcel(Parcel in) {
            return new Places(in);
        }

        @Override
        public Places[] newArray(int size) {
            return new Places[size];
        }
    };
}