package com.tripplan.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.tripplan.R;
import com.tripplan.model.Places;
import com.tripplan.network.RetrofitController;
import com.tripplan.util.CommonUtils;

import java.util.List;

public class SplashActivity extends Activity implements IPlacesInterface {
    private String latitude, longitude;
    public static final int REQUEST_LOCATION = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (!CommonUtils.hasPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

            CommonUtils.requestPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_LOCATION);


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            } else {
                LocationManager locationManager = (LocationManager) this
                        .getSystemService(Context.LOCATION_SERVICE);
                for (String locationProvider : locationManager.getAllProviders()) {
                    Location location = locationManager
                            .getLastKnownLocation(locationProvider);
                    if (location != null) {
                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());
                        break;
                    }
                }
            }
        }


        RetrofitController retrofitController = new RetrofitController();
        retrofitController.start(this, latitude, longitude, 0);
    }

    @Override
    public void onServiceSuccess(List<Places> placesList) {
        if (placesList != null && placesList.size() > 0) {
            Intent intent = PlacesActivity.createIntent(this, placesList);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onServiceFailure(String msg) {
        Log.e("::Service Error", msg);
        RetrofitController retrofitController = new RetrofitController();
        retrofitController.startMock(this, latitude, longitude, 0);
    }

}
