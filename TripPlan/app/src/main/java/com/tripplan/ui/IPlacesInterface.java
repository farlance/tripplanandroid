package com.tripplan.ui;

import com.tripplan.model.Places;

import java.util.List;

public interface IPlacesInterface {

    void onServiceSuccess(List<Places> placesList);

    void onServiceFailure(String msg);
}
