package com.tripplan.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tripplan.R;
import com.tripplan.model.Places;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlacesViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Places> mPlaces = new ArrayList<>();
    private PlacesViewHolder mPlacesViewHolder;
    private Context mContext;

    public PlacesViewAdapter(Context context, List<Places> places) {
        this.mPlaces = places;
        this.mContext = context;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mPlacesViewHolder = (PlacesViewHolder) holder;
        try {
            Drawable d = Drawable.createFromStream(mContext.getAssets().open(mPlaces.get(position).getImgUrl()), null);
            mPlacesViewHolder.mPlacesImage.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlacesViewHolder.mPlacesDesc.setText(mPlaces.get(position).getPlaceDisp());
        mPlacesViewHolder.mPlacesDistance.setText(mPlaces.get(position).getDistance());
        mPlacesViewHolder.mPlaceName.setText(mPlaces.get(position).getPlaceName());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_item_layout, parent, false);
        return new PlacesViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    private class PlacesViewHolder extends RecyclerView.ViewHolder {

        ImageView mPlacesImage;
        TextView mPlacesDesc, mPlacesDistance, mPlaceName;

        public PlacesViewHolder(View itemView) {
            super(itemView);
            mPlacesImage = itemView.findViewById(R.id.placeImageView);
            mPlacesDesc = itemView.findViewById(R.id.placeDescriptionTextView);
            mPlacesDistance = itemView.findViewById(R.id.placeDistanceText);
            mPlaceName = itemView.findViewById(R.id.placeNameText);
        }
    }

    public void changeByCategoryData(List<Places> placesList) {
        this.mPlaces.clear();
        this.mPlaces.addAll(placesList);
        notifyItemInserted(this.mPlaces.size());
    }
}
