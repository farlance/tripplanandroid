package com.tripplan.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tripplan.R;
import com.tripplan.model.Places;
import com.tripplan.util.Constants;
import com.tripplan.R;

import java.util.ArrayList;
import java.util.List;

public class PlacesFragment extends Fragment {

    private LinearLayoutManager mLayoutManager;
    private PlacesViewAdapter mPlacesViewAdapter;
    private PlaceViewHolder viewHolder;
    private List<Places> mPlacesLists = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_places, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewHolder = new PlaceViewHolder((ViewGroup) view);
        initRecyclerView();
    }

    private class PlaceViewHolder {
        RecyclerView mPlacesRecyclerView;

        PlaceViewHolder(ViewGroup parent) {
            mPlacesRecyclerView = parent.findViewById(R.id.places_recycler_view);
        }
    }

    private void initRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        viewHolder.mPlacesRecyclerView.setLayoutManager(mLayoutManager);
        viewHolder.mPlacesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        viewHolder.mPlacesRecyclerView.setNestedScrollingEnabled(false);
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.recyclerview_divider));
        viewHolder.mPlacesRecyclerView.addItemDecoration(divider);
        mPlacesViewAdapter = new PlacesViewAdapter(getActivity(), getArguments().getParcelableArrayList(Constants.KEY_PLACES_LIST));
        viewHolder.mPlacesRecyclerView.setAdapter(mPlacesViewAdapter);
        mPlacesViewAdapter.notifyDataSetChanged();
    }

    public void onNavigationMenuChanged(List<Places> placesList) {
        if (mPlacesViewAdapter != null && placesList != null && placesList.size() > 0) {
            mPlacesViewAdapter.changeByCategoryData(placesList);
            mPlacesViewAdapter.notifyDataSetChanged();
        }
    }
}
