package com.tripplan.ui;

import com.tripplan.model.Places;

import java.util.List;

public interface INavMenuInterface {

    void onNavigationMenuChanged(List<Places> placesList);
}
