package com.tripplan.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tripplan.R;
import com.tripplan.model.DatabaseHelper;
import com.tripplan.model.Places;
import com.tripplan.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class PlacesActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = PlacesActivity.class.getSimpleName();
    private PlacesFragment placesFragment;
    private DrawerLayout drawer;
    private DatabaseHelper mPlaceDatabaseHelper;
    private List<Places> mPlacesList = new ArrayList<Places>();
    private String selectedPlan = Constants.BEACH_PLAN;

    public static final Intent createIntent(@NonNull final Context context, List<Places> placesList) {
        Intent intent = new Intent(context, PlacesActivity.class);
        intent.putParcelableArrayListExtra(Constants.KEY_PLACES_LIST, (ArrayList<? extends Parcelable>) placesList);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mPlaceDatabaseHelper = new DatabaseHelper(this);
        placesFragment = new PlacesFragment();
        Bundle bundle = new Bundle();
        if (getIntent().getParcelableArrayListExtra(Constants.KEY_PLACES_LIST) != null) {
            mPlacesList = getIntent().getParcelableArrayListExtra(Constants.KEY_PLACES_LIST);
            mPlaceDatabaseHelper.insertPlaces(mPlacesList);
        }
        mPlacesList = mPlaceDatabaseHelper.getAllPlaces();
        bundle.putParcelableArrayList(Constants.KEY_PLACES_LIST, (ArrayList<? extends Parcelable>) mPlacesList);
        placesFragment.setArguments(bundle);
        loadFragment(placesFragment);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            updateView(R.string.beach_holiday_menu);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_beach) {
            updateView(R.string.beach_holiday_menu);
        } else if (id == R.id.nav_mountain) {
            updateView(R.string.mountain_menu);
        } else if (id == R.id.nav_tourism) {
            updateView(R.string.history_tourism_menu);
        } else if (id == R.id.nav_trekking) {
            updateView(R.string.trekking_steps_menu);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadFragment(PlacesFragment fragment) {

        drawer.closeDrawers();

        Runnable mPendingRunnable = () -> {
            // update the main content by replacing fragments
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment, TAG);
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        new Handler().post(mPendingRunnable);

        invalidateOptionsMenu();
    }

    private List<Places> getPlaces() {
        List<Places> placesList = new ArrayList<Places>();
        placesList = mPlaceDatabaseHelper.getAllPlaces();
        return placesList;
    }

    private List<Places> getPlacesByCategory(String category) {
        List<Places> _placesListObj = new ArrayList<Places>();
        List<Places> _placesList = getIntent().getParcelableArrayListExtra(Constants.KEY_PLACES_LIST);
        for (int i = 0; i < _placesList.size(); i++) {
            if (_placesList.get(i).getCategory().equalsIgnoreCase(category)) {
                _placesListObj.add(_placesList.get(i));
            }
        }
        return _placesListObj;
    }

    private void updateView(int id) {
        List<Places> placesList = getPlacesByCategory(getResources().getString(id));
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (fragment instanceof PlacesFragment) {
            ((PlacesFragment) fragment).onNavigationMenuChanged(placesList);
        }
    }
}
