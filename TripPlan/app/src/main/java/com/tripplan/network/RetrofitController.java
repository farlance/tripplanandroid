package com.tripplan.network;

import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tripplan.BuildConfig;
import com.tripplan.TripApplication;
import com.tripplan.model.PlacesList;
import com.tripplan.model.PlacesRequest;
import com.tripplan.ui.IPlacesInterface;

import java.security.InvalidParameterException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitController implements Callback<PlacesList> {

    static final String BASE_URL = "http://localhost:3000/api/";
    private IPlacesInterface mPlacesView;

    public void start(IPlacesInterface view, String latitude, String longitude, int value) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        /**Retrofit retrofit = new Retrofit.Builder()
         .baseUrl(BASE_URL)
         .addConverterFactory(GsonConverterFactory.create(gson))
         .build();*/

        mPlacesView = view;
        ServiceInterface serviceInterface = TripApplication.getRetrofitClient().create(ServiceInterface.class);

        Call<PlacesList> call = serviceInterface.getPlacesList(new PlacesRequest(value,latitude, longitude));
        call.enqueue(this);


    }

    public void startMock(IPlacesInterface view, String latitude, String longitude, int value) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        /**Retrofit retrofit = new Retrofit.Builder()
         .baseUrl(BASE_URL)
         .addConverterFactory(GsonConverterFactory.create(gson))
         .build();*/

        mPlacesView = view;
        ServiceInterface serviceInterface = TripApplication.getMockRetrofitClient().create(ServiceInterface.class);

        Call<PlacesList> call = serviceInterface.getPlacesList(new PlacesRequest(value,latitude, longitude));
        call.enqueue(this);


    }

    @Override
    public void onResponse(Call<PlacesList> call, Response<PlacesList> response) {
        if (response.isSuccessful()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.d("Service Success", response.body().toString());
                mPlacesView.onServiceSuccess(response.body().getPlacesList());
            }
        } else {
            Log.e("Service Error", response.errorBody().toString());
            mPlacesView.onServiceFailure(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<PlacesList> call, Throwable t) {
        mPlacesView.onServiceFailure(t.getMessage());
    }

    /*
           Build the Retrofit for mock httpclient
        */
    public static Retrofit build(@NonNull String baseUrl, OkHttpClient httpClientBuild) {
        if (TextUtils.isEmpty(baseUrl) || !URLUtil.isValidUrl(baseUrl)) {
            throw new InvalidParameterException("No URL was passed.");
        }

        Log.v("RetrofitController", "Reinitializing URL: " + baseUrl);

        GsonBuilder gsonBuilder = new GsonBuilder();

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gsonBuilder.create());

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(baseUrl);
        builder.addConverterFactory(gsonConverterFactory);

        return builder.client(httpClientBuild).build();
    }

    public static Retrofit build(@NonNull String baseUrl) {
        if (TextUtils.isEmpty(baseUrl) || !URLUtil.isValidUrl(baseUrl)) {
            throw new InvalidParameterException("No URL was passed.");
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(logging);
        }

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gsonBuilder.create());

        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(baseUrl);
        builder.addConverterFactory(gsonConverterFactory);

        return builder.client(httpClient.build()).build();
    }


}
