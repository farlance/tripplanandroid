package com.tripplan.network;

import com.tripplan.model.PlacesList;
import com.tripplan.model.PlacesRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceInterface {
    @POST("getPlaces")
    Call<PlacesList> getPlacesList(@Body PlacesRequest placesRequest);
}
