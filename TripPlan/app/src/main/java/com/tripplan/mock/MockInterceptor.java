package com.tripplan.mock;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MockInterceptor implements Interceptor {
    private static final String FILE_EXTENSION = ".json";
    private static final String MOCK_DIRECTORY = "mockdata/";
    private Context mContext;

    public MockInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        String method = chain.request().method().toLowerCase();

        Response response = null;
        // Get Request URI.
        final URI uri = chain.request().url().uri();
        Log.d("Mock", "--> Request url: [" + method.toUpperCase() + "]" + uri.toString());

        String defaultFileName = getFileName(chain);

        Log.d("Mock", "Read data from file: " + defaultFileName);
        try {
            InputStream is = mContext.getAssets().open(defaultFileName);
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            StringBuilder responseStringBuilder = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                responseStringBuilder.append(line).append('\n');
            }
            Log.d("Mock", "Response: " + responseStringBuilder.toString());
            Response.Builder builder = new Response.Builder();
            builder.request(chain.request());
            builder.protocol(Protocol.HTTP_1_0);
            String mContentType = "application/json";
            builder.addHeader("content-type", mContentType);

            builder.body(ResponseBody.create(MediaType.parse(mContentType), responseStringBuilder.toString().getBytes()));
            builder.code(200);
            builder.message(responseStringBuilder.toString());

            response = builder.build();
        } catch (IOException e) {
            Log.d("Mock", e.getMessage(), e);
        }

        Log.d("Mock", "<-- END [" + method.toUpperCase() + "]" + uri.toString());
        return response;
    }

    /*
        Get the file name from the URL
     */
    private String getFileName(Chain chain) {
        String fileName = chain.request().url().pathSegments().get(chain.request().url().pathSegments().size() - 1);

        return MOCK_DIRECTORY + fileName + FILE_EXTENSION;
    }
}