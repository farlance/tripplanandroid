package com.tripplan;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.tripplan.mock.MockInterceptor;
import com.tripplan.network.RetrofitController;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class TripApplication extends Application {

    private static TripApplication appInstance;
    private static Retrofit retrofitClient;

    private static Retrofit mockRetrofitClient;

    public static TripApplication getInstance() {
        return appInstance;
    }

    public static void rebuildRetrofitClient(@NonNull final Context context) {
        String url = "http://localhost:3000/api/";
        retrofitClient = RetrofitController.build(url);
    }

    public static void rebuildMockRetrofitClient(@NonNull final Context context) {
        String url = "http://localhost:3000/api/";

        mockRetrofitClient = RetrofitController.build(url, httpClient().build());
    }

    public static Retrofit getRetrofitClient() {
        return retrofitClient;
    }

    public static Retrofit getMockRetrofitClient() {
        return mockRetrofitClient;
    }

    /**
     * Build the HTTP Client to call PayFlexMockInterceptor for getting the mock data
     */
    private static OkHttpClient.Builder httpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        /*
            Set log levels for debug builds to monitor the HTTP Service calls
         */
        if (com.tripplan.BuildConfig.DEBUG) {
            logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

            httpClient.addInterceptor(logging);
        }
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(5, TimeUnit.MINUTES);
        httpClient.writeTimeout(5, TimeUnit.MINUTES);
        httpClient.readTimeout(5, TimeUnit.MINUTES);
        httpClient.addInterceptor(new MockInterceptor(getInstance()));
        return httpClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

        rebuildRetrofitClient(this);

        //For Mock
        rebuildMockRetrofitClient(this);
    }
}